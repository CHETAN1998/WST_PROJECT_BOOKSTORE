<?php
  $title = "Contact";
  require_once "./template/header.php";
?>
    <div class="row">
        <div class="col-md-3"></div>
		<div class="col-md-6 text-center">
			<form class="form-horizontal" method="post" action="customer_verify.php">
			  	<fieldset>
				    <legend>Contact</legend>
				    <p class="lead">Sign in</p>
				    <div class="form-group">
				      	<label for="inputEmail" class="col-lg-2 control-label">Email</label>
				      	<div class="col-lg-10">
				        	<input type="text" name="inputEmail" class="form-control" id="inputEmail" placeholder="Email">
				      	</div>
				    </div>
				    <div class="form-group">
				      	<label for="password" class="col-lg-2 control-label">Password</label>
				        <input type="password" name="pass">
				    </div>
				    <div class="form-group">
				      	<div class="col-lg-10 col-lg-offset-2">
						<button type="Sign in" name="Sign_in" class="btn btn-primary">Sign in</button>
				        	<button type="reset" class="btn btn-default">Cancel</button>
						<br><a href="sign_up.php">Not a member?</a>
				      	</div>
				    </div>
			  	</fieldset>
			</form>
		</div>
		<div class="col-md-3"></div>
    </div>
<?php
  require_once "./template/footer.php";
?>
