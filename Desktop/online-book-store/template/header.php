<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

    <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="./bootstrap/css/jumbotron.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Arvo" rel="stylesheet"> 
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Home</a>
        </div>

        <!--/.navbar-collapse -->
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
              <!-- link to publiser_list.php -->
              <li><a href="publisher_list.php"><span class="glyphicon glyphicon-paperclip"></span>&nbsp; Publisher</a></li>
              <!-- link to books.php -->
              <li><a href="books.php"><span class="glyphicon glyphicon-book"></span>&nbsp; Books</a></li>
	      <!-- link to contacts.php -->
	<?php
if(!isset($_SESSION['logged'])) { ?> 
	<li><a href="contact.php"><span class="glyphicon glyphicon-log-in"></span>&nbsp; Sign in</a></li>
<?php
}
?>
<?php
if(isset($_SESSION['logged']) && $_SESSION['logged'] == true) { ?> 
<li><a href="customer_logout.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Log out</a></li>
<?php
}
?>
              <li><a href="cart.php"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp; My Cart</a></li>
<?php
if(!isset($_SESSION['logged'])) { ?> 
              <li><a href="admin.php"><span class="glyphicon glyphicon-user"></span>&nbsp; Admin Login</a></li>
<?php
}
else {
?>
	<li><a><span class="glyphicon glyphicon-user"></span>&nbsp; Welcome <?php echo $_SESSION['user']; ?></a></li>
<?php
}
?>
            </ul>
        </div>
	<div>
        	<form role="search" method="POST" action="search.php">
            	<input type="text" class="form-control" name="keyword" style="width:100%;margin:20px 10% 20px 0%;" placeholder="Search for a Book , Author">
        	</form>
        </div>
      </div>
    </nav>
    <?php
      if(isset($title) && $title == "Index") {
    ?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="margin-top:15px">
      <div class="container" style="margin-left:30%">
        <h1 style="align:center">Welcome to bookstore</h1>
      </div>
    </div>
    <?php } ?>

    <div class="container" id="main">
